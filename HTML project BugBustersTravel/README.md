Alpha 22 QA - Module 2

Group HTML Page Task

Requirements

Create one starting group HTML page;
All group members should contribute;
You are free to decide the topic of your page (hobbies, achievements, etc.);
Use class and id to identify and select your HTML elements;
You may add an additional group page as an "easter egg";
Each member should create one "personal" HTML page related to the common topic;
Build some basic navigation between the documents;
Create a common CSS file and use it across all HTML documents;
Each member should contribute with at least 2 CSS selectors;
You may have additional CSS for your personal HTML pages;
Create a common JavaScript file to handle basic events (onClick events etc.);
You may have additional .js files for your personal pages;
Using AJAX is optional;
You must create and develop this project in your group Telerik GitLab repos;
Use meaningful commit messages;
Do not override each others code (replacing files or copy-pasting whole folder);
Communicate.

Presentation;

Each group will have 20 minutes to present their project;
Each member should present his/her personal page;
Presentations will be held during the 11 August session.

Last modified: Wednesday, 29 July 2020, 1:37 PM
