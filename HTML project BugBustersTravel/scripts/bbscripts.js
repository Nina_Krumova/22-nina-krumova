
(() => {
    const btnTxtFirst = document.getElementById("btnTxtFirst");
    btnTxtFirst.addEventListener("click",(event)=>{
        console.log(event);
        showHideText("textFirst");
    });

    const btnTxt = document.getElementById("btnTxt");
    btnTxt.addEventListener("click",(event)=>{
        console.log(event);
        showHideText("text");
    });
})();

const showHideText = (elementId) => {
    const text = document.getElementById(elementId);
    if(text.style.display === "none" || text.style.display === undefined || text.style.display === ""){
        text.style.display = "block";
    }else{
        text.style.display = "none";
    };
};

function Clock() {
    let rtClock = new Date();

    let hours = rtClock.getHours();
    let minuts = rtClock.getMinutes();
    let seconds = rtClock.getSeconds();

    hours = ("0" + hours).slice(-2);
    minuts = ("0" + minuts).slice(-2);
    seconds = ("0" + seconds).slice(-2);
    
    document.getElementById(`clock`).innerHTML = 
    hours + " : " + minuts + " : " + seconds;
    let t = setTimeout(Clock, 500);
}
