package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Car;
import com.telerikacademy.dealership.models.contracts.Comment;

import java.util.List;

public class CarImpl extends VehicleBase implements Car {

    private static final int MIN_SEATS = 1;
    private static final int MAX_SEATS = 10;
    int seats;

    public CarImpl(String make, String model, double price, int seats) {
        super(make, model, price, VehicleType.CAR, 4);
        setSeats(seats);
    }

    public void setSeats(int seats) {
        if (seats < MIN_SEATS || seats > MAX_SEATS) {
            throw new IllegalArgumentException(String.format("Seats must be between %d and %d!",
                    MIN_SEATS, MAX_SEATS));
        }
        this.seats = seats;
    }

    @Override
    protected String printAdditionalInfo() {
        return "";
    }

    @Override
    public int getSeats() {
        return this.seats;
    }

    @Override
    public String toString() {
            return super.toString()
                    + "Seats: " + seats + System.lineSeparator()
                    + this.printComments();
        }
    }

