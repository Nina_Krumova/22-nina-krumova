package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Motorcycle;

import java.util.List;

public class MotorcycleImpl extends VehicleBase implements Motorcycle {

    public static final int MIN_CATEGORY_CHARACTERS = 3;
    public static final int MAX_CATEGORY_CHARACTERS = 10;
    String category;

    public MotorcycleImpl(String make, String model, double price, String category) {
        super(make, model, price, VehicleType.MOTORCYCLE, 2);
        setCategory(category);
    }

    public void setCategory(String category) {
        if (category.length() < MIN_CATEGORY_CHARACTERS || category.length() > MAX_CATEGORY_CHARACTERS) {
            throw new IllegalArgumentException(String.format("Category must be between %d and %d characters long!",
                    MIN_CATEGORY_CHARACTERS, MAX_CATEGORY_CHARACTERS));
        }
        this.category = category;
    }

    @Override
    protected String printAdditionalInfo() {
        return "";
    }

    @Override
    public String getCategory() {
        return this.category;
    }

    @Override
    public String toString() {
        return super.toString()
                + "Category: " + category + System.lineSeparator()
                + this.printComments();
    }
}
