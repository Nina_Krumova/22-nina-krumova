package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Truck;

import java.util.List;


public class TruckImpl extends VehicleBase implements Truck {

    public static final int MIN_WEIGHT_CAPACITY = 1;
    public static final int MAX_WEIGHT_CAPACITY = 100;
    int weightCapacity;

    public TruckImpl(String make, String model, double price, int weightCapacity) {
        super(make, model, price, VehicleType.TRUCK, 8);
        setWeightCapacity(weightCapacity);
    }

    public void setWeightCapacity(int weightCapacity) {
        if (weightCapacity < MIN_WEIGHT_CAPACITY || weightCapacity > MAX_WEIGHT_CAPACITY) {
            throw new IllegalArgumentException(String.format("Weight capacity must be between %d and %d!",
                    MIN_WEIGHT_CAPACITY, MAX_WEIGHT_CAPACITY));
        }
        this.weightCapacity = weightCapacity;
    }

    @Override
    protected String printAdditionalInfo() {
        return "";
    }

    @Override
    public int getWeightCapacity() {
        return this.weightCapacity;
    }

    @Override
    public String toString() {
        return super.toString()
                + "Weight Capacity: " + weightCapacity + "t" + System.lineSeparator()
                + this.printComments();

    }
}
