package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Utils;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Vehicle;

import java.util.ArrayList;
import java.util.List;

public abstract class VehicleBase implements Vehicle {

    private final static String MAKE_FIELD = "Make";
    private final static String MODEL_FIELD = "Model";
    private final static String PRICE_FIELD = "Price";
    private final static String WHEELS_FIELD = "Wheels";
    private final static String COMMENTS_HEADER = "    --COMMENTS--";
    private final static String NO_COMMENTS_HEADER = "    --NO COMMENTS--";
    private static final int MIN_MAKE_CHARACTERS = 2;
    private static final int MAX_MAKE_CHARACTERS = 15;
    private static final int MIN_PRICE = 0;
    private static final int MAX_PRICE = 1000000;
    public static final int MIN_MODEL_LENGTH = 1;
    public static final int MAX_MODEL_LENGTH = 15;

    //add fields
    protected List<Comment> comments;
    protected String make;
    protected String model;
    protected Double price;
    protected VehicleType vehicleType;
    int wheels;
    
    // finish the constructor and validate input; look in package com.telerikacademy.dealership.models.common.enums; what methods are there in VehicleType?
    public VehicleBase(String make, String model, double price, VehicleType vehicleType, int wheels) {

        setMake(make);
        setModel(model);
        setPrice(price);
        setType(vehicleType);
        setWheels(wheels);
        comments = new ArrayList<>();
    }

    @Override
    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        if (make.length() < MIN_MAKE_CHARACTERS || make.length() > MAX_MAKE_CHARACTERS) {
            throw new IllegalArgumentException(String.format("Make must be between %d and %d characters long!",
                    MIN_MAKE_CHARACTERS, MAX_MAKE_CHARACTERS));
        }
        this.make = make;
    }

    @Override
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        if (model.length() < ModelsConstants.MIN_MODEL_LENGTH || model.length() > ModelsConstants.MAX_MODEL_LENGTH) {
            throw new IllegalArgumentException(String.format("Model name must be between %d and %d characters long!",
                    MIN_MODEL_LENGTH, MAX_MODEL_LENGTH));
        }
        this.model = model;
    }

    @Override
    public double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        if (price < MIN_PRICE || price > MAX_PRICE) {
            throw new IllegalArgumentException(String.format("Price must be between %d and %d!",
                    MIN_PRICE, MAX_PRICE));
        }
        this.price = price;
    }



    @Override
    public VehicleType getType() {
        return this.vehicleType;
    }

    public void setType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    @Override
    public int getWheels() {
        return wheels;
    }

    public void setWheels(int wheels) {
        this.wheels = wheels;
    }

    @Override
    public void removeComment(Comment comment) {
        comments.remove(comment);
    }

    @Override
    public void addComment(Comment comment) {
        comments.add(comment);
    }

    @Override
    public List<Comment> getComments() {
        return this.comments;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        
        builder.append(String.format("%s:", this.getClass().getSimpleName().replace("Impl", ""))).append(System.lineSeparator());
        //finish implementation of toString() - what should the next 3 lines of code append to the builder?
        builder.append(String.format("Make: %s", make)).append(System.lineSeparator());
        builder.append(String.format("Model: %s", model)).append(System.lineSeparator());
        builder.append(String.format("Wheels: %s", wheels)).append(System.lineSeparator());
        
        builder.append(String.format("%s: $%s", PRICE_FIELD, Utils.removeTrailingZerosFromDouble(price))).append(System.lineSeparator());
        
        if (!printAdditionalInfo().isEmpty()) {
            builder.append(printAdditionalInfo()).append(System.lineSeparator());
        }
     //   builder.append(printComments());
        return builder.toString();
    }
    
    //todo replace this comment with explanation why this method is protected:
    protected abstract String printAdditionalInfo();
    
    protected String printComments() {
        StringBuilder builder = new StringBuilder();
        
        if (comments.size() <= 0) {
            builder.append(String.format("%s", NO_COMMENTS_HEADER));
        } else {
            builder.append(String.format("%s", COMMENTS_HEADER)).append(System.lineSeparator());
            
            for (Comment comment : comments) {
                builder.append(comment.toString());
            }
            
            builder.append(String.format("%s", COMMENTS_HEADER));
        }
        
        return builder.toString();
    }
    
}
