import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import javax.swing.*;
import java.security.Key;

public class GoogleSearchTests {

    private static final String googleSearchUrl = "https://google.com";
    private WebDriver driver;

    @BeforeClass
    public static void classInit(){
        System.setProperty("webdriver.chrome.driver", "D:\\Documents\\Courses\\Telerik\\Drivers\\chromedriver.exe");
    }

    @Before
    public void testInit(){
        driver = new ChromeDriver();
        driver.get(googleSearchUrl);
        agreeWithConsent(driver);
    }

     @After
    public void testCleanup() {
        driver.close();
    }

    @Test
    public void googlePageHomePageNavigated_When_GoogleOpened(){
        String expectedHomePageUrl = "https://www.google.com/";
        Assert.assertEquals("Page was not navigated", driver.getCurrentUrl(), expectedHomePageUrl);
    }

    @Test
    public void googleSearchResultPageNavigated_When_SearchIsPerformed(){
        WebElement searchTextBox = driver.findElement(By.xpath("//input[@name='q']"));
     //   driver.findElement(By.name("q"));
        searchTextBox.sendKeys("Telerik Academy Alpha" + Keys.ENTER);

  /*     WebElement suggestionsSection = driver.findElement(By.className("UUbT9"));
       WebElement searchButton = driver.findElement(By.name("btnK"));

        wait(1000);

        new Actions(driver).moveToElement(searchButton)
                .click()
                .perform();

        wait(1000);

     //   searchButton.click();*/

        Assert.assertTrue(
                "Page was not navigated.Actual url: " + driver.getCurrentUrl() + ". Expected url: "
                        + "https://www.google.com/search", driver.getCurrentUrl().contains("https://www.google.com/search"));
    }

    private void wait(Long timeout) {
        try {
            Thread.sleep(timeout);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void agreeWithConsent(WebDriver driver) {
        WebElement iFrame = driver.findElement(By.xpath("//div[@id='cnsw']")).findElement(By.tagName("iframe"));
        driver.switchTo().frame(iFrame);
        WebElement agreeButton = driver.findElement(By.id("introAgreeButton"));
        agreeButton.click();
        driver.switchTo().defaultContent();
    }
}
