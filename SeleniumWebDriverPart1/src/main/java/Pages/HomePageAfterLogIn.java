package Pages;

import Utils.Browser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.time.LocalDateTime;

public class HomePageAfterLogIn {


    @FindBy(id = "create-topic")
    public static WebElement newTopicButton;

    @FindBy(id = "reply-title")
    public static WebElement topicTitleBox;

    @FindBy(xpath = "//textarea[@placeholder='Type here. Use Markdown, BBCode, or HTML to format. Drag or paste images.']")
    public static WebElement topicContentBox;

    @FindBy(xpath = "//button[@aria-label='Create Topic']")
    public static WebElement createTopicButton;

    public static void newTopicCreation (String topicTitle, String topicContent){
        LocalDateTime dateTime = LocalDateTime.now();
        newTopicButton.click();
        topicTitleBox.click();
        topicTitleBox.sendKeys( topicTitle + " " + dateTime.toString());
        topicContentBox.click();
        topicContentBox.sendKeys(topicContent +" " + dateTime.toString());
        createTopicButton.click();
    }

}
