package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import Utils.Browser;
import org.openqa.selenium.support.FindBy;

import java.util.concurrent.TimeUnit;

public class LogInPage {

    public final static String EMAIL = "ninak_test@abv.bg";
    public final static String PASSWORD = "@12345678";

    @FindBy(id = "Email")
    public static WebElement emailTextBox;

    @FindBy(id = "Password")
    public static WebElement password;

    @FindBy(id = "next")
    public static WebElement signInButton;

    public static void logIn(){

        emailTextBox.click();
        emailTextBox.sendKeys(LogInPage.EMAIL);
        password.click();
        password.sendKeys(LogInPage.PASSWORD);
        signInButton.click();
    }

}
