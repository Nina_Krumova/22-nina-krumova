package Pages;

import Utils.Browser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProfileSubMenu {

    @FindBy(xpath = "//a[@class=\"widget-link user-activity-link\"]")
    public static WebElement profileName;

    @FindBy(xpath = "//li[@class=\"logout read\"]")
    public static WebElement logOutButton;

    public static void clickProfileName(){
        ProfileSubMenu.profileName.click();
    }

    public static void clickLogOut(){
        ProfileSubMenu.logOutButton.click();
    }

}
