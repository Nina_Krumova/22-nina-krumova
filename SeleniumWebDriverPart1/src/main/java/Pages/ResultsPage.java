package Pages;

import Utils.Browser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ResultsPage {

    @FindBy(xpath = "(//a[@class=\"search-link\"])[1]")
    public static WebElement findFoundTopic;

    public static void clickTheFoundTopic(){

        findFoundTopic.click();

    }

}
