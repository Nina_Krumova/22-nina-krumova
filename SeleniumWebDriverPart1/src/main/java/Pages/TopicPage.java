package Pages;

import Utils.Browser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class TopicPage {

    @FindBy(xpath = "//button[@class=\"widget-button btn-flat show-more-actions no-text btn-icon\"]")
    public static WebElement showMoreButton;

    @FindBy(xpath = "//button[@aria-label=\"delete topic\"]")
    public static WebElement deleteButton;

    public static void deleteTopic(){
        TopicPage.showMoreButton.click();
        TopicPage.deleteButton.click();
    }
}
