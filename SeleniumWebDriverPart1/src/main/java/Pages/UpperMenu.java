package Pages;

import Utils.Browser;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class UpperMenu {

    public static ProfileSubMenu profileSubMenu;

    @FindBy(xpath = "//li[@class='header-dropdown-toggle search-dropdown']//a[@class='icon btn-flat']//*[local-name()='svg']")
    public static WebElement searchButton;

    @FindBy(xpath = "//a[@class=\"icon\"]")
    public static WebElement profileIcon;

    @FindBy(id = "search-term")
    public static WebElement searchTextField;


    public static void logOut(){
        PageFactory.initElements(Browser.driver, ProfileSubMenu.class);
        profileIcon.click();
        profileSubMenu.profileName.click();
        profileSubMenu.logOutButton.click();
    }

    public static void searchForTopic(String topicTitle) {
        searchButton.click();
        searchTextField.click();
        searchTextField.sendKeys(topicTitle + Keys.ENTER);
    }

    public static void clickProfileIcon(){
        UpperMenu.profileIcon.click();
    }

}
