import Pages.*;
import Utils.Browser;
import Utils.Waiting;
import org.junit.*;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class ForumFunctionalitiesTest {

    private WebDriverWait wait;
    public static final String topicTitle = "I like the sea";
    public static final String topicContent = "I like testing with SeleniumWebDriver";

    @Before
    public void testInit(){
        Browser.init("chrome");
        wait = new WebDriverWait(Browser.driver, 3);

        PageFactory.initElements(Browser.driver, HomePageBeforeLogIn.class);
        HomePageBeforeLogIn.logIn.click();

        PageFactory.initElements(Browser.driver, LogInPage.class);
        LogInPage.logIn();
    }

    @Test
    public void telerikForumTopicCreation_When_ForumOpened(){
        PageFactory.initElements(Browser.driver, HomePageAfterLogIn.class);
        HomePageAfterLogIn.newTopicCreation(topicTitle,topicContent);

        Assert.assertTrue("Topic was not created", Browser.driver.getPageSource().contains(topicTitle));
    }

    @Test
    public void telerikForumTopicDeletion_When_TopicWasCreated(){
        PageFactory.initElements(Browser.driver, HomePageAfterLogIn.class);
        PageFactory.initElements(Browser.driver, ResultsPage.class);
        PageFactory.initElements(Browser.driver, TopicPage.class);
        PageFactory.initElements(Browser.driver, UpperMenu.class);

        UpperMenu.searchForTopic(topicTitle);
        ResultsPage.clickTheFoundTopic();
        TopicPage.deleteTopic();

        Assert.assertTrue("Deletion was not successful", Browser.driver.getPageSource().contains("topic withdrawn by author, will be automatically deleted in 24 hours unless flagged"));
    }

    @After
    public void testLogOut(){
        PageFactory.initElements(Browser.driver, UpperMenu.class);
        PageFactory.initElements(Browser.driver, ProfileSubMenu.class);
        PageFactory.initElements(Browser.driver, LogInPage.class);

        Waiting.waitFor(1000);
        UpperMenu.clickProfileIcon();
        Waiting.waitFor(1000);
        ProfileSubMenu.clickProfileName();
        Waiting.waitFor(1000);
        ProfileSubMenu.clickLogOut();
        Waiting.waitFor(1000);

        wait.until(ExpectedConditions.elementToBeClickable(HomePageBeforeLogIn.logIn));
        Browser.driver.close();
    }
}
