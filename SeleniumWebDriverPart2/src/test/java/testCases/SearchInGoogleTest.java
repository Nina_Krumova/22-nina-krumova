package testCases;

import com.telerikacademy.testframework.Utils;
import org.junit.Test;

public class SearchInGoogleTest extends BaseTest {
	String searchCriterion= "Telerik Academy";
	@Test
	public void simpleGoogleSearch() {
		actions.switchIFrame("google.iFrame");
		actions.waitForElementVisible("agree.Button",10);
		actions.clickElement("agree.Button");
		Utils.getWebDriver().switchTo().defaultContent();
		actions.typeValueInField(searchCriterion, "search.Input");
		actions.waitForElementVisible("search.Button",10);
		actions.clickElement("search.Button");
		actions.waitForElementVisible("search.Result",10);
		actions.clickElement("search.Result");

		navigateToQACourseViaCard();

		actions.assertNavigatedUrl("academy.QASignUpUrl");
	}

	private void navigateToQACourseViaCard(){
		actions.clickElement("academy.AlphaAnchor");
		actions.clickElement("academy.AcceptCookieButton");
		actions.clickElement("academy.QaGetReadyLink");
		actions.clickElement("academy.SignUpNavButton");
	}
}
