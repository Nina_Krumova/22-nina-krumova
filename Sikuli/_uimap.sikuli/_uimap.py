########################################################
# UI map for XYZ
########################################################
from sikuli import *
########################################################

class ForumUI:
    WindowTextField = "WindowTextField.png"
    Login = "Login-button.png"
    Email = "EmailField.png"
    Password = "PasswordField.png"
    SignIn = "SignIn-button.png"
    NewTopic = "NewTopic-button.png"
    TitleTopic = "TitleField.png"
    TopicText = "TextFiled.png"
    CreateTopic = "CreateTopic-button.png"
    CreatedTopic = "CreatedTopic.png"
    Dots = "Dots-button.png"
    Bin = "Bin.png"
    TopicDeleted = "TopicDeleted.png"
    ProfileButton = "Profile-button.png"
    ProfileName = "ProfileName.png"
    Logout = "LogOut-button.png"
    LearningPlatform = "LearningPlatform-button.png"
    User = "User-button.png"
    LearningPlatformLogout = "LearningPlatformLogout-button.png"