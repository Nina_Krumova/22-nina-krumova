from _lib import *
from datetime import datetime

    
class SmokeTests(unittest.TestCase):
    
    def setUp(self):
        pass
    
    def tearDown(self):
        pass    
        
    def test_001_OpenForum(self):
        Forum.Start()
        
    def test_010_Test_CreateTopic(self):

        click (ForumUI.WindowTextField)
        wait (3)
        type ('https://stage-forum.telerikacademy.com/' + Key.ENTER)
        wait (3)
	click (ForumUI.Login)
	wait (3)
	click (ForumUI.Email)
	type ('ninak_test@abv.bg')
	click (ForumUI.Password)
	type ('@12345678')
	click (ForumUI.SignIn)
	wait (3)
	click (ForumUI.NewTopic)
	wait (3)
	click (ForumUI.TitleTopic)
	type ('Test topic NinaK ' + datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
	click (ForumUI.TopicText)
	type ('I like testing ' + datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
	wait (3)
	click (ForumUI.CreateTopic)

        assert(exists(ForumUI.CreatedTopic))

    def test_020_Test_DeleteTopic(self):
        click(ForumUI.Dots)
        wait(3)
        click(ForumUI.Bin)
	wait (3)

        assert(exists(ForumUI.TopicDeleted))

	click (ForumUI.ProfileButton)
	click (ForumUI.ProfileName)
	click (ForumUI.Logout)
	wait (3)
	click (ForumUI.LearningPlatform)
	click (ForumUI.User)
	click (ForumUI.LearningPlatformLogout)
	wait (3)
            

    def test_100_CloseForum(self):
        Forum.Close()

if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(SmokeTests)
    
    print("WARNING: You may need to adapt UI map to match your windows specifics")
    outfile = open("Report.html", "w")
    runner = HTMLTestRunner.HTMLTestRunner(stream=outfile, title='SmokeTests Report' )
    runner.run(suite)
    outfile.close()

