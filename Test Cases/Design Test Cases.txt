Create test cases covering the following functionality of a forum:
1. Comments;
2. Creation of topics;

1.1. Title - Write comments in a forum.

1.2. Narrative:
	- To share opinion on a topic
	- As a user of 'BG mamma'
	- I want to write a comment in awready created topic.

1.3. Steps to reproduce:
	- Log in with your account;
	- Select the topic in which you want to post a comment;
	- Click "Reply" button;
	- In the textbox write a comment;
	- Click "publish" button.


2.1. Title - Create topics in a forum.

2.2. Narrative:
	- Тo gather information from personal experience on an issue that I'm interested in
	- As a user of 'BG mamma'
	- I want to create a new topic.

2.3. Steps to reproduce:
	- Log in with your account;
	- Click New Topic;
	- Type your topic title in the Title box;
	- Type your topic body in the Post box;
	- Click Save to post your topic.
