# Test Plan
## Testing types
* Functional Testing
* Automated Regression Testing
* Smoke testing
* Usability testing

##  Schedule
* Manual testing – 3 days
* Functional Testing – 1 day
* Automated Regression Testing – 1 day
* Smoke testing – 1 day
* Usability testing – 0,5 day

## Scope of testing
### Functionalities to be tested
* Login in the forum
* Create new topic
* Create posts in the new created topic
* Edit new topic
* Delete new topic

### Functionalities not to be tested
* User creation
* Comments
* User settings

## Exit Criteria
* 100% of priority 1 test cases are covered and pass
* Priority 2 test cases are executed and 80% pass
* When all blocking,critical and high bugs are fixed and verified
* Time for testing run out