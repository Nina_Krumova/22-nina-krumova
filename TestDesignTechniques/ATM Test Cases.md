``` 1. Equivalence partitioning:```

#Test Case 1:
* Title: Withdraw BGN 200 from ATM 
* Narrative: I want to withdraw BGN 200 from ATM
* Pre-conditions: BGN 1000 in the account
* Steps to reproduce: 
  1. Insert the card
  2. Select language
  3. Select the type of Transaction - Withdrawal of Money
  4. Select the withdrawal amount - BGN 200
  5. Enter the PIN
  6. Press confirm
    
* Expected result: The withdrawal of BGN 200 should be successfully
* Priority: 1



#Test Case 2:
* Title: Withdraw BGN 5 from ATM
* Narrative: I want to withdraw BGN 5 from ATM
* Pre-conditions: BGN 1000 in the account
* Steps to reproduce: 
  1. Insert the card
  2. Select language
  3. Select the type of Transaction - Withdrawal of Money
  4. Enter the withdrawal amount - BGN 5
  
* Expected result: The ATM should display an error that the minimum withdrawal amount is BGN 10. 
* Priority: 3



#Test Case 3:
* Title: Withdraw 1050 BGN from ATM in several transactions
* Narrative: I want to withdraw BGN 1050 from ATM
* Pre-conditions: BGN 1000 in the account
* Steps to reproduce: 
  1. Insert the card
  2. Select language
  3. Select the type of Transaction - Withdrawal of Money
  4. Enter the withdrawal amount - BGN 1050

* Expected result: The ATM should display an error that there isn't enough balance in the account. 
* Priority: 1

----------------------------------------------------------------------------------------------------

```2. Boundary Values:```

#Test Case 4:
* Title: Withdraw 0 BGN from ATM
* Narrative: I want to withdraw BGN 0 from ATM
* Pre-conditions: BGN 1000 in the account
* Steps to reproduce: 
  1. Insert the card
  2. Select language
  3. Select the type of Transaction - Withdrawal of Money
  4. Enter the withdrawal amount - BGN 0
    
* Expected result: The ATM should display an error that the amount is invalid. 
* Priority: 2



#Test Case 5:
* Title: Withdraw 2000 BGN from ATM in several transactions
* Narrative: I want to withdraw BGN 2000 from ATM
* Pre-conditions: BGN 2000 in the account; The ATM is set for maximum 2 transactions per day for BGN 800 each, for a particular card
* Steps to reproduce: 
  1. Insert the card
  2. Select language
  3. Select the type of Transaction - Withdrawal of Money
  4. Enter the withdrawal amount - BGN 2000
    
* Expected result: The ATM should display an error that the limit is exceeded for number of transactions. 
* Priority: 1

----------------------------------------------------------------------------------------------------

```3. PIN entering part:```

#Test Case 6:
* Title: Enter correct PIN code for withdraw money from ATM
* Narrative: I want to withdraw BGN 20 from ATM
* Pre-conditions: BGN 100 in the account
* Steps to reproduce: 
  1. Insert the card
  2. Select language
  3. Select the type of Transaction - Withdrawal of Money
  4. Enter the withdrawal amount - BGN 20
  5. Enter correct PIN
  6. Press confirm
    
* Expected result: The withdrawal of BGN 20 should be successfully, after entering the correct PIN  
* Priority: 1



#Test Case 7:
* Title: Enter incorrect PIN code for withdraw money from ATM
* Narrative: I want to withdraw BGN 20 from ATM
* Pre-conditions: BGN 100 in the account
* Steps to reproduce: 
  1. Insert the card
  2. Select language
  3. Select the type of Transaction - Withdrawal of Money
  4. Enter the withdrawal amount - BGN 20
  5. Enter incorrect PIN
 
    
* Expected result: The ATM should display an error that the PIN is invalid.  
* Priority: 1

-----------------------------------------------------------------------------------------------------

```5. Use Case Testing to think of end-to-end scenarios for ATM Usage:```

 5.1. Verify the ATM machine accepts card.
 5.2. Verify the error message by inserting an invalid card (Expired Card).
 5.3. Verify that there is an action like blocking of card occurs when the total nomber of incorrect PIN attempts get surpassed.
 5.4. Verify the machine logs out of the user session immediately after successful withdrawal.
 5.5. Verify the message when there is no money in the ATM.
 5.6. Verify the language selection functionality.
 5.7. Verify the ATM machine successfully takes out the money.
 5.8. Verify the ATM machine takes out the balance printout after the withdrawal.
 5.9. Verify the text on the screen buttons visible clearly.
 5.10. Verify the functionality of all the buttons on the keypad.
 5.11. Verify that touch of the ATM screen is operational.
 5.12. Verify the user is allowed to choose different account types like Deposits, Current Balance, etc.
 5.13. Verify the functionality of the receipt printer.
 5.14. Verify whether the printed data is correct or not in the receipt.
 5.15. Verify how much time the system takes to log out.