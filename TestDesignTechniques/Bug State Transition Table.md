
| State | Event | Action | New state |
| ------ | ------ |------ |------ |
| ------ | New bug is found |Create a new bug | New |
| New | Click button Assign | Add assignee | Assigned |
| Assigned | Click button Reject | Reject issue | Rejected |
| Rejected | Click button Reopen | Return for work | Re-open |
| Assigned | Click button Working | Start progress | In progress |
| In progress | Click button Resolved | Complete | Resolved |
| Resolved | Click button Testing | Start testing | Veriffication |
| Veriffication | Click button Reopen | Return for work | Re-open |
| Veriffication | Click button Close the issue | Verified | Done |
| Done | Click button Reopen | Return for work | Re-open |
| Re-open | Click button assign | Add assignee | Assigned |
