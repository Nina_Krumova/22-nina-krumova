
import com.telerikacademy.MyArrayList;
import com.telerikacademy.MyList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MyArrayListTests {

    private MyList<Integer> list;

    @BeforeEach
    public void init(){
        list = new MyArrayList<>();
    }

    @Test
    public void get_should_returnRightElement_when_IndexIsValid() {
        list.add(1);
        list.add(2);
        Integer result = list.get(0);
        Assertions.assertEquals(1, result);
    }

    @Test
    public void get_should_throw_when_indexIsBelowZero(){
    Assertions.assertThrows(IndexOutOfBoundsException.class,
            ()-> list.get(-1));
    }
    @Test
    public void get_should_throw_when_indexIsAboveUpperBound(){
        Assertions.assertThrows(IndexOutOfBoundsException.class,
                ()-> list.get(0));
    }
    @Test void set_should_setValue_when_indexIsValid(){
        list.add(1);
        list.add(3);
        Integer value = 2;
        list.set(0, value);
        Assertions.assertEquals(value, list.get(0));
    }
    @Test void getLast_should_returnLastElement(){
        list.add(1);
        list.add(2);
        list.add(3);
        Integer result = list.getLast();
        Assertions.assertEquals(3, result);
    }
    @Test void getFirst_should_returnFirstElement(){
        list.add(1);
        list.add(2);
        list.add(3);
        Integer result = list.getFirst();
        Assertions.assertEquals(1, result);
    }
    @Test void getSize_should_returnArrayLength_whenLessThanFourElements(){
        list.add(1);
        list.add(2);
        list.add(3);
        Integer result = list.getSize();
        Assertions.assertEquals(4, result);
    }
    @Test void getSize_should_returnArrayLength_whenMoreThanFourElements(){
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(3);
        list.add(3);
        Integer result = list.getSize();
        Assertions.assertEquals(8, result);
    }
    @Test void constructorWithElements_should_addAllElements(){
        list = new MyArrayList<Integer>(new Integer[]{1, 2, 3});
        Assertions.assertEquals(1, list.get(0));
        Assertions.assertEquals(2, list.get(1));
        Assertions.assertEquals(3, list.get(2));
    }
    @Test void findIndexOf_should_returnRightIndex_when_ElementIsValid(){
        list.add(1);
        list.add(2);
        list.add(3);
        Integer result = list.findIndexOf(2);
        Assertions.assertEquals(1, result);
    }
    @Test void findIndexOf_should_returnRightIndex_when_ElementIsInvalid(){
        list.add(1);
        list.add(2);
        list.add(3);
        Integer result = list.findIndexOf(4);
        Assertions.assertEquals(-1, result);
    }
}
