import com.telerikacademy.users.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class UserTests {

    @Test public void getUsername_should_setAndGetTheName(){

        String username = "Nina";
        User result = new User(username);
        Assertions.assertEquals("Nina", result.getUsername());
    }
    @Test public void setUsername_should_throw_when_usernameIsAboveMaxLength(){
        Assertions.assertThrows(IllegalArgumentException.class,
                ()-> new User("IvanaIvanovaIvanovaIvanova"));
    }
    @Test public void setUsername_should_throw_when_usernameIsBelowMinLength(){
        Assertions.assertThrows(IllegalArgumentException.class,
                ()-> new User(""));
    }

}
